# README

This project holds sources for the website published at
https://tnl-project.gitlab.io and https://tnl-project.org/

## Set up a development environment

1. Make sure that you have the [Git LFS](https://git-lfs.com/) extension
   installed.

2. Clone the repository and enter its directory:

    ```
    git clone https://gitlab.com/tnl-project/tnl-project.gitlab.io.git
    cd tnl-project.gitlab.io
    ```

3. Create a Python virtual environment:

    ```
    python -m venv .venv
    source .venv/bin/activate
    ```

4. Install required Python packages in the virtual environment:

    ```
    pip install -r requirements.txt
    ```

5. Start a development server for Nikola that continuously watches the source
   tree and rebuilds the site as necessary:

    ```
    nikola auto
    ```

## Help with Nikola

See the [Nikola handbook](https://getnikola.com/handbook.html).
