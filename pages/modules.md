The TNL project comprises several _domain-specific modules_ that target mainly
computational fluid dynamics.
All modules listed below have **full multi-GPU support** based on the common
core library.

<div class="card mb-4" markdown="1">
<h4 class="card-header">TNL-MHFEM</h4>
<div class="card-body" markdown="1">
![](/files/logos/modules/tnl-mhfem-logo.png){: height=100 .rounded .float-right .m-2 }

[TNL-MHFEM](https://gitlab.com/tnl-project/tnl-mhfem) is an implementation of
the __Mixed Hybrid Finite Element Method__. The numerical scheme is implemented
for a PDE system written in the general coefficient with 8 problem-specific
coefficients.

The scheme was originally developed for simulating multicomponent flow and
transport phenomena in porous media, but it can be used for any problem whose
governing equations can be written in a compatible form.

</div>
</div>


<div class="card mb-4" markdown="1">
<h4 class="card-header">TNL-LBM</h4>
<div class="card-body" markdown="1">
![](/files/logos/modules/tnl-lbm-BL.png){: height=100 .rounded .float-right .m-2 }

[TNL-LBM](https://gitlab.com/tnl-project/tnl-lbm) is an implementation of the
__Lattice Boltzmann Method__. It is a
high-performance lattice Boltzmann code for direct numerical simulations (DNS)
of turbulent flow. It was verified and validated on multiple problems.

**The main features are:**

- Modular architecture with pluggable components (collision operators,
  streaming patterns, boundary conditions, macroscopic quantities, etc).
  The implemented components include the [cumulant collision operator][CuLBM]
  for D3Q27 and the [A-A pattern][A-A pattern] streaming scheme that can
  significantly reduce memory consumption.
- Optimized data layout on uniform lattice based on the [NDArray][NDArray]
  data structure from TNL.
- Scalable distributed computing based on [CUDA-aware MPI][CUDA-aware MPI] and
  [DistributedNDArraySynchronizer][DistributedNDArraySynchronizer] from TNL.
  Good parallel efficiency is ensured by overlapping computation and
  communication, pipelining and optimized data layout.
- Coupling with the _immersed boundary method_.

[CuLBM]: https://doi.org/10.1016/j.camwa.2015.05.001
[A-A pattern]: https://doi.org/10.1109/ICPP.2009.38
[NDArray]: https://tnl-project.gitlab.io/tnl/ug_NDArrays.html
[DistributedNDArraySynchronizer]: https://tnl-project.gitlab.io/tnl/classTNL_1_1Containers_1_1DistributedNDArraySynchronizer.html
[CUDA-aware MPI]: https://developer.nvidia.com/blog/introduction-cuda-aware-mpi/
</div>
</div>


<div class="card mb-4" markdown="1">
<h4 class="card-header">TNL-SPH</h4>
<div class="card-body" markdown="1">
![](/files/logos/modules/tnl-sph-logo.png){: height=100 .rounded .float-right .m-2 }

[TNL-SPH](https://gitlab.com/tnl-project/tnl-sph) is an implementation of the
__Smoothed Particle Hydrodynamics Method__. It is a high-performance code for
numerical simulations of free surface flow. It is verified an validated on
standard SPH benchmarks, comparing the provided results with available
experimental data.

**The main features are:**

- Modular architecture with pluggable components (formulations, schemes,
  diffusive terms, viscous terms, boundary conditions etc). The implementation
  includes δ-WCSPH, Boundary Integrals WCSPH and Riemann SPH formulations, and
  various inlet and outlet boundary conditions.
- Optimized and efficient framework for general particle simulations allowing
  implementation of other particle methods.
- Distributed multi-GPU computing based on [CUDA-aware MPI][CUDA-aware MPI] and
  1D domain decomposition using domain overlaps.

</div>
</div>

Other utility modules provide support for the development of the project, e.g.
*benchmarks*, *Python bindings*, etc. The complete list of TNL modules can be
found on [GitLab](https://gitlab.com/tnl-project).
