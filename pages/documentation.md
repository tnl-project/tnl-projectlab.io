To start using TNL we recommend proceeding in the following steps:

1. [Installation instructions](https://tnl-project.gitlab.io/tnl/index.html#installation) provide step-by-step guidance on how to install TNL on your system, ensuring a smooth setup process.
2. [Usage hints](https://tnl-project.gitlab.io/tnl/index.html#usage) describe the most convenient techniques for compiling and building projects using TNL.
3. [Users' guide](https://tnl-project.gitlab.io/tnl/UsersGuide.html) contains a comprehensive explanation of the data structures and algorithms provided by TNL.
4. [Video tutorials on Youtube](https://www.youtube.com/@GPUProgrammingWithTNL) explain basic concepts and use of data structures and algorithms provided by TNL.


# Papers

- Klinkovský J., Trautz A. C., Fučík R., Illangasekare T. H., [_Lattice
  Boltzmann method–based efficient GPU simulator for vapor transport in the
  boundary layer over a moist soil: Development and experimental
  validation_](https://doi.org/10.1016/j.camwa.2023.02.021), Computers &
  Mathematics with Applications vol. 138, pp. 65-87, 2023.

- Klinkovský J., Oberhuber T., Fučík R., Žabka V., [_Configurable open-source
  data structure for distributed conforming unstructured homogeneous meshes with
  GPU support_](https://dl.acm.org/doi/10.1145/3536164), ACM Transactions on
  Mathematical Software, vol. 48, no.3, article No. 30, pp. 1–30, 2022.

- Oberhuber T., Klinkovský J., Fučík R., [_TNL: Numerical library for modern
  parallel architectures_](https://doi.org/10.14311/AP.2021.61.0122), Acta
  Polytechnica, vol. 61, no. SI, pp. 122-134, 2020.

- Eichler P., Malík M., Oberhuber T., Fučík R., [_Numerical investigation of the
  discrete solution of phase-field equation_](
  http://www.iam.fmph.uniba.sk/amuc/ojs/index.php/algoritmy/article/view/1565),
  Proceedings of Algoritmy 2020, pp. 111-120, 2020.

- Fučík R., Klinkovský J., Solovský J., Oberhuber T., Mikyška J.,
  [_Multidimensional Mixed–Hybrid Finite Element Method for Compositional
  Two–Phase Flow in Heterogeneous Porous Media and its Parallel Implementation
  on GPU_](https://doi.org/10.1016/j.cpc.2018.12.004), Computer Physics
  Communications, vol. 238, pp.165-180, 2019, doi.org/10.1016/j.cpc.2018.12.004.

# Theses

- Vorobyev, I., [_Implementation of advanced parallel algorithms for
  multidimensional arrays in the TNL library_](
  https://dspace.cvut.cz/handle/10467/120364),
  bachelor's thesis, Faculty of Nuclear Sciences and Physical Engineering,
  CTU in Prague, 2025.

- Salaba, M., [_Implementation of algorithms for calculating matrix eigenvalues
  in the TNL library_](https://dspace.cvut.cz/handle/10467/116895),
  bachelor's thesis, Faculty of Nuclear Sciences and Physical Engineering,
  CTU in Prague, 2024.

- Cichra, R., [_Parallel graph algorithms for GPU_](
  https://dspace.cvut.cz/handle/10467/116912), bachelor's thesis, Faculty of
  Nuclear Sciences and Physical Engineering, CTU in Prague, 2024.

- Krastenov, A., [_Parallel algorithms for operations with dense matrices_](
  https://dspace.cvut.cz/handle/10467/116917), bachelor's thesis, Faculty
  of Nuclear Sciences and Physical Engineering, CTU in Prague, 2024.

- Novotný, V., [_Parallel algorithms for operations with sparse matrices_](
  https://dspace.cvut.cz/handle/10467/116916), bachelor's thesis, Faculty
  of Nuclear Sciences and Physical Engineering, CTU in Prague, 2024.

- Kolesnik, I., [_Implementation of data structure for adaptive hierarchical
  meshes in the TNL library_](https://dspace.cvut.cz/handle/10467/114686),
  master's thesis, Faculty of Information Technology, CTU in Prague, 2024.

- Čejka, L., [_Parallel Computation of LU Decomposition on GPUs for the
  Numerical Solution of Partial Differential Equations_](
  https://dspace.cvut.cz/handle/10467/111604), master's thesis, Faculty of
  Nuclear Sciences and Physical Engineering, CTU in Prague, 2023.

- Král, P., [_Polyhedral mesh optimization for better accuracy of numerical
  computations_](https://dspace.cvut.cz/handle/10467/111525),
  bachelor's thesis, Faculty of Nuclear Sciences and Physical Engineering,
  CTU in Prague, 2023.

- Groschaft, J., [_Parallel algorithms for data hashing on GPUs_](
  https://dspace.cvut.cz/handle/10467/101044),
  master's thesis, Faculty of Information Technology, CTU in Prague, 2022.

- Hayeu, Y., [_Parallel computations on orthogonal grids on GPU_](
  https://dspace.cvut.cz/handle/10467/102081), bachelor's thesis, Faculty of
  Information Technology, CTU in Prague, 2022.

- Bobot, J., [_Implementation of data structure for polyhedral numerical meshes
  in TNL_](https://dspace.cvut.cz/handle/10467/99479), master's thesis, Faculty
  of Information Technology, CTU in Prague, 2022.

- Nguyen, X. T., [_Development of parallel sorting algorithms for GPU_](
  https://dspace.cvut.cz/handle/10467/95433), bachelor's thesis, Faculty of
  Information Technology, CTU in Prague, 2021.

- Kolushev, A., [_Algorithms and data structures for hashing on GPU_](
  https://dspace.cvut.cz/handle/10467/92872), bachelor's thesis, Faculty of
  Information Technology, CTU in Prague, 2021.

- Kolesnik, I., [_Implementation and comparison of formats for storage of
  sparse matrices in TNL library_](https://dspace.cvut.cz/handle/10467/92866),
  bachelor's thesis, Faculty of Information Technology, CTU in Prague, 2021.

- Duong, T. D., [_Implementation of B-trees on GPU_](
  https://dspace.cvut.cz/handle/10467/96740), bachelor's thesis, Faculty of
  Information Technology, CTU in Prague, 2021.

- Fencl, M., [_Parallel algorithms of linear algebra on GPU_](
  https://dspace.cvut.cz/handle/10467/98465), master's thesis, Faculty of
  Nuclear Sciences and Physical Engineering, CTU in Prague, 2020.

- Schäfer, J., [_Implementation of numerical schemes for simulation of
  compressible turbulent flow_](https://dspace.cvut.cz/handle/10467/83329),
  master's thesis, Faculty of Nuclear Sciences and Physical Engineering,
  CTU in Prague, 2019.

- Schäfer, J., [_Implementation of numerical schemes for simulation of inviscid
  compressible flow_](https://dspace.cvut.cz/handle/10467/75733),
  bachelor's thesis, Faculty of Nuclear Sciences and Physical Engineering,
  CTU in Prague, 2017.

- Novotný, M., [_Extended presicion arithmetic on GPU_](
  https://dspace.cvut.cz/handle/10467/8863), bachelor's thesis, Faculty of
  Nuclear Sciences and Physical Engineering, CTU in Prague, 2012.

# Check out our presentations on TNL

[2024, Current Problems in Numerical Analysis, Institute of Mathematics
Czech Academy of Sciences](/files/pdfs/2024-institute-of-mathematics-cas.pdf){: .btn .btn-outline-primary .btn-block }

[2022, LBM Kindergarten school in Krakow, Poland](/files/pdfs/lbm-krakow.pdf){: .btn .btn-outline-primary .btn-block }

[2019, ICNAAM 2019, Rhodos, Greece](/files/pdfs/tnl-icnaam-new.pdf){: .btn .btn-outline-primary .btn-block }

[2019, IMG2019, Kunming, China](/files/pdfs/tnl-img2019.pdf){: .btn .btn-outline-primary .btn-block }

[2019, WSC, Děčín, Czech Republic](/files/pdfs/tnl-wsc.pdf){: .btn .btn-outline-primary .btn-block }

[2019, HPCSE, Karolinka, Czech Republic](/files/pdfs/tnl-hpcse.pdf){: .btn .btn-outline-primary .btn-block }

[2018, CSASC, Bratislava, Slovak republic](/files/pdfs/tnl-csasc-2018.pdf){: .btn .btn-outline-primary .btn-block }

[2017, Acomen, Ghent, Belgium](/files/pdfs/17-acomen-gent.pdf){: .btn .btn-outline-primary .btn-block }

[2016, CJPS, Krakow, Poland](/files/pdfs/16-cjps-krakow.pdf){: .btn .btn-outline-primary .btn-block }

[2016, WSC, Děčín, Czech republic](/files/pdfs/16-wsc-decin.pdf){: .btn .btn-outline-primary .btn-block }

[2016, Algoritmy, Podbanské, Slovak republic](/files/pdfs/16-algoritmy-podbanske.pdf){: .btn .btn-outline-primary .btn-block }

[2015, WSC, Děčín, Czech republic](/files/pdfs/15-wsc-decin.pdf){: .btn .btn-outline-primary .btn-block }

[2014, PUMPS, Barcelona, Spain](/files/pdfs/14-pumps-barcelona.pdf){: .btn .btn-outline-primary .btn-block }
