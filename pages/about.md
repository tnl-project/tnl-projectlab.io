# Our Team

<div class="card mb-4" markdown="1">
<h4 class="card-header">Tomáš Oberhuber</h4>
<div class="card-body" markdown="1">
![](/files/people/TomasOberhuber-300x300.jpg){: height=100 .rounded .float-left .m-2 }
is an associate professor at the [Department of Mathematics](https://km.fjfi.cvut.cz),
[Faculty of Nuclear Science and Physical Engineering](https://www.fjfi.cvut.cz)
at [Czech Technical University in Prague](https://www.cvut.cz), former head of
the CUDA research center at CTU in Prague. His domains of interest are
numerical mathematics, image processing, mathematical optimization, HPC and
programming in C++. He started development of TNL in 2004 and he is responsible
for the overall design of TNL.
</div>
<div class="card-footer">Project leader</div>
</div>

<div class="card mb-4" markdown="1">
<h4 class="card-header">Jakub Klinkovský</h4>
<div class="card-body flex-row" markdown="1">
![](/files/people/Jakub_Klinkovsky.png){: height=100 .rounded .float-left .m-2 }
is Ph.D. graduate from the [Department of Mathematics](https://km.fjfi.cvut.cz)
and a researcher at the [Department of Software Engineering](https://ksi.fjfi.cvut.cz),
[Faculty of Nuclear Science and Physical Engineering](https://www.fjfi.cvut.cz)
at [Czech Technical University in Prague](https://www.cvut.cz). His domains of
interest are numerical mathematics, multiphase porous media flow, HPC and
programming in C++ and Python. In TNL, he has implemented many data structures
such as unstructured meshes and algorithms such as CWYGMRES. He is the main
developer of the [TNL-MHFEM](https://gitlab.com/tnl-project/tnl-mhfem),
[TNL-LBM](https://gitlab.com/tnl-project/tnl-lbm), and
[PyTNL](https://gitlab.com/tnl-project/pytnl) modules.
</div>
<div class="card-footer">Main developer</div>
</div>

<div class="card mb-4" markdown="1">
<h4 class="card-header">Tomáš Halada</h4>
<div class="card-body" markdown="1">
![](/files/people/Tomas_Halada.jpg){: height=100 .rounded .float-left .m-2 }
is a Ph.D. student at the [Department of Technical Mathematics](https://mat.fs.cvut.cz),
[Faculty of Mechanical Engineering](https://www.fs.cvut.cz/en/home/) at
[Czech Technical University in Prague](https://www.cvut.cz). His domains of
interest are numerical mathematics and particle methods with applications to
problems in continuum mechanics. In TNL, he focuses on development of module for
Smoothed Particle Hydrodynamics method.
</div>
<div class="card-footer">Developer</div>
</div>

<div class="card mb-4" markdown="1">
<h4 class="card-header">Radek Fučík</h4>
<div class="card-body" markdown="1">
![](/files/people/Fucik.jpg){: height=100 .rounded .float-left .m-2 }
is an associate professor at the [Department of Mathematics](https://km.fjfi.cvut.cz)
and the head of the [Department of Software Engineering](https://ksi.fjfi.cvut.cz),
[Faculty of Nuclear Science and Physical Engineering](https://www.fjfi.cvut.cz) at
[Czech Technical University in Prague](https://www.cvut.cz). His domains of
interest include the mathematical modelling of fluid flow and the transport of
substances in free and porous media. He is also involved in the development and
mathematical analysis of the Lattice Boltzmann method, and the development and
application of the mixed hybrid finite element method (MHFEM). He is the
co-founder of the [TNL-MHFEM](https://gitlab.com/tnl-project/tnl-mhfem) and
[TNL-LBM](https://gitlab.com/tnl-project/tnl-lbm) modules.
</div>
<div class="card-footer">Developer</div>
</div>

<div class="card mb-4" markdown="1">
<h4 class="card-header">Pavel Eichler</h4>
<div class="card-body" markdown="1">
![](/files/people/Eichler.jpg){: height=100 .rounded .float-left .m-2 }
is a researcher at the [Department of Software Engineering](https://ksi.fjfi.cvut.cz),
[Faculty of Nuclear Science and Physical Engineering](https://www.fjfi.cvut.cz)
at [Czech Technical University in Prague](https://www.cvut.cz). His main domain
of interest is mathematical modelling of fluid flow and transport. Particularly,
he focuses on the development, analysis, and application of the Lattice Boltzmann
method. He is a developer of the [TNL-LBM](https://gitlab.com/tnl-project/tnl-lbm)
module.
</div>
<div class="card-footer">Developer</div>
</div>

<div class="card mb-4" markdown="1">
<h4 class="card-header">Aleš Wodecki</h4>
<div class="card-body" markdown="1">
![](/files/people/ales-272x300.jpg){: height=100 .rounded .float-left .m-2 }
is a researcher at the [Faculty of Electrical Engineering](https://intranet.fel.cvut.cz/en/)
and [Faculty of Nuclear Science and Physical Engineering](https://www.fjfi.cvut.cz)
at [Czech Technical University in Prague](https://www.cvut.cz). His domains of
interest are optimization methods, materials science and quantum computing.
</div>
<div class="card-footer">Developer</div>
</div>

## Current developers

- __Ilya Stupachenko (FNSPE, CTU in Prague)__ -- parallel graph algorithms.
- __Valeriia Zvezdina  (FNSPE, CTU in Prague)__ -- operations with sparse matrices.
- __Iryna Spizhavka (FNSPE, CTU in Prague)__ -- optimization methods.
- __Radek Cichra  (FNSPE, CTU in Prague)__ -- parallel graph algorithms.
- __Alexandr Krastenov (FNSPE, CTU in Prague)__ -- operations with dense matrices.
- __Kirill Tiuliusin (FNSPE, CTU in Prague)__ -- hashing algorithms.
- __Samuel Križan (FNSPE, CTU in Prague)__ -- algorithms for machine learning.

## Former developers

- __Ilya Vorobyev (FNSPE, CTU in Prague)__ -- algorithms for multidimensional arrays.
- __Marek Salaba (FNSPE, CTU in Prague)__ -- algorithms for calculating matrix eigenvalues.
- __Vít Novotný (FNSPE, CTU in Prague)__ -- operations with sparse matrices.
- __Ilja Kolesnik (FIT, CTU in Prague)__ -- GPU kernels for CSR format for sparse matrices, adaptive grids.
- __Lukáš Čejka (FNSPE, CTU in Prague)__ -- sparse matrix formats and LU decomposition.
- __Yury Hayeu (FIT, CTU in Prague)__ -- orthogonal grids and convolutions.
- __Jan Groschaft (FIT, CTU in Prague)__ -- hashing algorithms for GPUs.
- __Ján Bobot (FIT, CTU in Prague)__ -- polyhedral numerical meshes.
- __Tat Dat Duong (FIT, CTU in Prague)__ -- B-trees for GPUs.
- __Xuan Thang Nguyen (FIT, CTU in Prague)__ -- sorting algorithms (quicksort and bitnoic sort) for GPUs.
- __Askar Kolushev (FIT, CTU in Prague)__ -- hashing algorithms for GPUs.
- __Matouš Fencl (FNSPE, CTU in Prague)__ -- sparse matrices and solvers for Hamilton-Jacobi equation.
- __Jan Schafer (FNSPE, CTU in Prague)__ -- solvers for compressible Navier-Stokes equations.
- __Vít Hanousek (FNSPE, CTU in Prague)__ -- distributed numerical grids.
- __Vítězslav Žabka (FNSPE, CTU in Prague)__ -- unstructured numerical meshes.
- __Tomáš Sobotík (FNSPE, CTU in Prague)__ -- solvers for Hamilton-Jacobi equation.
- __Libor Bakajsa (FNSPE, CTU in Prague)__ -- sparse matrix formats.
- __Ondřej Székely (FNSPE, CTU in Prague)__ -- solvers for parabolic problems.
- __Jan Vacata (FNSPE, CTU in Prague)__ -- sparse matrix formats.
- __Martin Heller (FNSPE, CTU in Prague)__ -- sparse matrix formats.
- __Matěj Novotný (FNSPE, CTU in Prague)__ -- high precision arithmetics.

## Graphic design

The TNL logo was designed by [DesignHUB](https://designhub.dk/).

![TNL logo](/files/logos/tnl/logo-text-blue.svg){: height=100 }
