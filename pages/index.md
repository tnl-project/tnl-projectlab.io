# Template Numerical Library

TNL is a collection of building blocks that facilitate the development of
efficient numerical solvers and HPC algorithms. It is implemented in C++ using
modern programming paradigms in order to provide a flexible and user-friendly
interface such as the STL library, for example. TNL provides native support
for modern hardware architectures such as multicore CPUs, GPUs, and
distributed systems, which can be managed via a unified interface.

[Join us at Gitlab](https://gitlab.com/tnl-project/tnl){: .btn .btn-primary }
[Support us at Open Collective](https://opencollective.com/tnl-project){: .btn .btn-primary }

# Highlights…

* [Unified memory management](https://tnl-project.gitlab.io/tnl/ug_Arrays.html)
  for CPUs and GPUs with
  [extended smart pointers](https://tnl-project.gitlab.io/tnl/ug_Pointers.html).
* BLAS-like functions enclosed to templated vectors and __expression templates__ for simple and efficient
  [vector operations](https://tnl-project.gitlab.io/tnl/ug_Vectors.html).
* [Various types of parallel for loops](https://tnl-project.gitlab.io/tnl/ug_ForLoops.html) for 1D, 2D and 3D ranges.
* [Flexible parallel reduction](https://tnl-project.gitlab.io/tnl/ug_ReductionAndScan.html)
  and [parallel scan](https://tnl-project.gitlab.io/tnl/ug_ReductionAndScan.html#autotoc_md86)
  for GPUs and multicore CPUs.
* [Parallel sorting](https://tnl-project.gitlab.io/tnl/ug_Sorting.html)
  including inplace sorting with user defined swapping.
* [Various types of matrices](https://tnl-project.gitlab.io/tnl/ug_Matrices.html)
  like dense, tridiagonal, multidiagonal and general sparse matrices based on
  number of **different formats**.
* [Lambda matrices](https://tnl-project.gitlab.io/tnl/ug_Matrices.html#autotoc_md39)
  with matrix elements defined by C++ lambda functions.
* [Segments](https://tnl-project.gitlab.io/tnl/ug_Segments.html)
  -- data **abstraction of sparse matrix formats** like CSR, Adaptive CSR,
  Ellpack, Sliced Ellpack, Chunked Ellpack, Bisection Ellpack.
* [Linear solvers](https://tnl-project.gitlab.io/tnl/ug_Linear_solvers.html)
  -- CG, BiCGStab,GMRES, parallel CWYGMRES, TFQMR, Jacobi, SOR.
* [Preconditioners](https://tnl-project.gitlab.io/tnl/ug_Linear_solvers.html)
  of linear solvers -- Jacobi, ILU0 (CPU only), ILUT (CPU only).
* [ODE solvers](https://tnl-project.gitlab.io/tnl/ug_ODE_solvers.html)
  -- 1st order (Euler, Midpoint), 2nd order (Heun, Ralston, Fehlberg), 3rd order (Kutta, Heun, Van der Houwen/Wray,
     Ralston, SSPRK3, Bogacki-Shampin), 4th order (Runge-Kutta, Ralston, Runge-Kutta-Merson), 5th order
     (Cash-Karp, Dormand-Prince, Fehlberg ), Matlab (ode1, ode2, ode23, ode45), including methods
     with adaptive choice of time step.
* [Orthogonal numerical grids](https://tnl-project.gitlab.io/tnl/ug_Grids.html)
  for efficient computation on structured orthogonal numerical grids, including
  **staggered grids**.
* [Unstructured numerical meshes](https://tnl-project.gitlab.io/tnl/ug_Meshes.html)
  (including distributed) with import from FPMA, Netgen, VTU, VTK, XMLVTK, PVTU
  and PVTK.
* Image formats -- DICOM (via DCMTK), JPEG (via libjpeg), PNG (via libpng), PGM.

## TNL modules

The TNL project comprises several _domain-specific modules_ that target mainly
computational fluid dynamics:

* [TNL-MHFEM](https://gitlab.com/tnl-project/tnl-mhfem) is an implementation of the
  **Mixed Hybrid Finite Element Method** which is suitable especially for
  simulations of multiphase and multicomponent flow and transport phenomena.
* [TNL-LBM](https://gitlab.com/tnl-project/tnl-lbm) is a high-performance
  implementation of the **Lattice Boltzmann Method** for numerical simulation of
  turbulent flow.
* [TNL-SPH](https://gitlab.com/tnl-project/tnl-sph) is a high-performance
  implementation of the **Smoothed Particle Hydrodynamics Method** for numerical
  simulation of free surface flow.

See the [Modules page](/pages/modules) for details about all TNL modules.

# Try out…

Template Numerical Library is provided under the terms of the [MIT License](
https://gitlab.com/tnl-project/tnl/blob/main/LICENSE).
To get the latest version of TNL, run the following command in your terminal:

```
git clone https://gitlab.com/tnl-project/tnl
```

Users of Arch Linux may install TNL from [Arch Linux User Repository](
https://aur.archlinux.org/packages/tnl-git/).

# Get involved… {: #contact }

TNL is an open-source project and everyone using the library is encouraged to
contribute their ideas and enhancements to the whole community. The TNL project
has a broad scope and some areas are covered better than others. If you have a
specific suggestion or a general question, please let us know in some of the
following communication channels.

[GitLab issues](https://gitlab.com/tnl-project/tnl/-/issues){: .btn .btn-primary }
[Merge requests](https://gitlab.com/tnl-project/tnl/-/merge_requests){: .btn .btn-primary }
[#tnl-project space at matrix.org](https://matrix.to/#/#tnl-project:matrix.org){: .btn .btn-primary }
[#tnl room at matrix.org](https://matrix.to/#/#tnl:matrix.org){: .btn .btn-primary }
[Facebook group](https://www.facebook.com/groups/tnlproject){: .btn .btn-primary }

## Citing

If you use TNL in your scientific projects, please cite the following papers in
your publications:

- T. Oberhuber, J. Klinkovský, R. Fučík, [TNL: Numerical library for modern parallel architectures](
  https://ojs.cvut.cz/ojs/index.php/ap/article/view/6075), Acta Polytechnica 61.SI (2021), 122-134.
- J. Klinkovský, T. Oberhuber, R. Fučík, V. Žabka, [Configurable open-source data structure for
  distributed conforming unstructured homogeneous meshes with GPU support](
  https://doi.org/10.1145/3536164), ACM Transactions on Mathematical Software, 2022, 48(3), 1-33.

# Partners and sponsors

[![Numfocus](/files/logos/Numfocus-AffiliatedProject-transparent.png){: height=100 }](https://numfocus.org/)

[![IHPCI](/files/logos/IHPCI-logo-transp.png){: height=180 }](https://www.ihpci.org/)

## This project was supported by…

- _Analysis of flow character and prediction of evolution in endovascular
  treated arteries by magnetic resonance imaging coupled with mathematical
  modeling_, project no. NV19-08-00071 of Ministry of Health of the Czech
  Republic, 2019-2022.
- _Research Center for Informatics,_ OPVVV project no.
  CZ.02.1.01/0.0/0.0/16_019/0000765 of Ministry of Education, Youth and Sports
  of the Czech Republic, 2018-2022.
- _Large structures in boundary layers over complex surfaces under high
  Reynolds numbers_, project no. 18-09539S of the Czech Science Foundation,
  2017-2019.
- _Quantitative Mapping of Myocard and of Flow Dynamics by Means of MR Imaging
  for Patients with Nonischemic Cardiomyopathy - Development of Methodology_,
  project No. 15-27178A of Ministry of Health of the Czech Republic, 2015-2018.

# Code of Conduct

The TNL project welcomes and encourages participation by everyone. Therefore,
we have adopted a code of conduct in order to create an open and welcoming
environment for everyone, regardless of age, body size, disability, ethnicity,
gender, level of experience, education, nationality, race, religion, or sexual
identity and orientation. No matter how you identify yourself or how others
perceive you, you are welcome in our community. For more details see…

[TNL Code of Conduct](https://mmg-gitlab.fjfi.cvut.cz/gitlab/tnl/tnl-dev/-/blob/develop/CODE_OF_CONDUCT.md){: .btn .btn-primary }
