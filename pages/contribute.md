The TNL project welcomes and encourages participation by everyone. While most
of the work for TNL involves programming in principle, we value and encourage
contributions even from people proficient in other, non-technical areas.

This section provides several ideas how both new and experienced TNL users can
contribute to the project. Note that this is not an exhaustive list.

* Join the code development. Our [GitLab issues tracker](https://gitlab.com/tnl-project/tnl/-/issues)
  contains many ideas for new features, or you may bring your own. The
  [contributing guidelines](https://gitlab.com/tnl-project/tnl/-/blob/develop/CONTRIBUTING.md)
  describe the standards for code contributions.
* Help with testing and reporting problems. Testing is an integral part of
  agile software development which refines the code development. Constructive
  critique is always welcome.
* Improve and extend the documentation. Even small changes such as improving
  grammar or fixing typos are very appreciated.
* Share your experience with TNL. Have you used TNL in your own project? Please
  be open and share your experience to help others in similar fields to get
  familiar with TNL. If you could not utilize TNL as smoothly as possible, feel
  free to submit a [feature request](https://gitlab.com/tnl-project/tnl/-/issues).
